import React from 'react'
import styled from 'styled-components'
import check from '../img/check.jpg'
const Container = styled.div`
    display:flex;
    justify-content:space-between;
    align-items:center;
    margin-left:15px;
`
const Title = styled.p`
    text-transform:uppercase;
    color:#fff;
    font-weight:bold;
    font-size:15px;

`
export const Button= styled.button`
    border-radius:50px;
    border:2px solid #fff;
    background-color:${props=> props.visible?'white':'transparent'};
    width:36px;
    height:35px;
    cursor: pointer;
`
export const Icon = styled.img`
    height:32px;
    width: 32px;
    object-fit:contain;
    border-radius:50px;
    margin-top:-2px;
    margin-left:-6px;
    visibility:${props=> props.visible?'visible':'hidden'};
`
export const Options = ({title,state,onChange}) => {

  
    const handleChange=()=>{
        if(typeof onChange ==='function') onChange(title)
    }

  return (
    <Container>
        <Title>
            {title} 
        </Title>
        <Button  onClick={handleChange}>
            <Icon src={check} visible={state} />
        </Button>
    </Container>
  )
}
