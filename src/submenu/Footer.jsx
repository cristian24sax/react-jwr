import React from 'react'
import styled from 'styled-components'
import { Button, Icon } from './Options'
import cancel from '../img/cancel.png'
import check from '../img/check.jpg'
import reset from '../img/reset.jpg'
const Container = styled.div`
    display:flex;
    justify-content:center;
    align-items:center;
    flex-direction:row;
    margin-top:20px;
    `
const Item =({v,onClick})=>{
    return(
    <Button visible={true} onClick={onClick} style={{marginLeft:15,marginTop:15, height:60, width:60,padding:3}}>
        <Icon visible={true} src={v} style={{height:60, width:60}}/>
    </Button>
    )
}
export const Footer = ({valor,onReset,onAcept,onCancel,id}) => {
    const handleReset =()=>{
        // console.log('resetenado')
        if (typeof onReset ==='function') onReset(id)
    }
    const handleOk =()=>{
        console.log('OK')
        if (typeof onAcept ==='function') onAcept()
    }
    const handleCancel =()=>{
        console.log('cancelando')
        if (typeof onCancel ==='function') onCancel()
    }



    if(valor===1){
        let data =[
            {icon:check,onClick:handleOk},
            {icon:reset,onClick:handleReset},
            {icon:cancel,onClick:handleCancel}
        ]
        return(
        <Container style={{borderTop:'1px solid #ffff'}}>
            {data.map( (v,i)=><Item key={i} v={v.icon} onClick={v.onClick}/>)}                       
        </Container>
        )
    }

    if (valor===2){
        let data =[
            {icon:check,onClick:handleOk},
            {icon:cancel,onClick:handleCancel}
        ]
        return(
            <Container >
            {data.map( (v,i)=><Item key={i} v={v.icon} onClick={v.onClick} />)}                       
            </Container>
            )
    } 
        

}
