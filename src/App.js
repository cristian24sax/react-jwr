import { Card } from "./components/Card";
import styled from 'styled-components'
import { Card2 } from "./components/Card2";
import { useState } from "react";
const Container = styled.div`
  margin:10px;
`
function App() {
  const [data, setData] = useState([
    {
    id:1,
    color:"#21d0d0", 
    options:[
        {title:'price low to high', state:false},
        {title:'price low to low', state:false},
        {title:'popularity', state:false}
      ],
      footer:2
    },
      
    {
    id:2,
    color:"#ff7745", 
    options:[
        {title:'1up nutrition', state:false},
        {title:'asitis', state:false},
        {title:'avatar', state:false},
        {title:'big muscles', state:false},
        {title:'bpn sports', state:false},
        {title:'bsn', state:false},
        {title:'cellucor', state:false},
        {title:'domin8r', state:false},
        {title:'dymatize', state:false},
      ],
      footer:1
    },
    ]);
  // console.log(data)

  const handleChange = (value,id)=>{
    const match = data.filter(el=>el.id===id)[0]
    const act = match.options.filter(el=>el.title===value)[0]
    const copy=[...match.options]

    const targetIndex = copy.findIndex(f=>f.title===value)
    if(targetIndex>-1){
      copy[targetIndex]={title:value,state:!act.state}
      match.options=copy
    }
    const copyList= [...data]
    setData(copyList) 
  }
  const handleReset = (id) =>{
    const copy=[...data]
    // const match = copy.filter(el=>el.id===id)[0]
    console.log(copy)
    const targetIndex = copy.findIndex(f=>f.id===id)
    console.log(targetIndex)
    if(targetIndex>-1){
      let raw=[];
      const options = data.filter(el=>el.id===id)[0].options
      // for(let x of options) raw.push({title:x.title,state:false})
      for(let x of options) raw =[...raw,{title:x.title,state:false}]
      
      copy[targetIndex].options=raw
    }
    setData(copy)

    


  }
  return (
    <Container>
      <Card/>
      {data.map((d,i)=><Card2 key={i} {...d} onChange={handleChange} onReset={handleReset}/>)}
      
    </Container>
  );
}

export default App;
