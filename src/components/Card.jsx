import React from 'react'
import styled from 'styled-components'
import { Boton } from './Boton'


const Container = styled.div`
    height:150px;
    width:350px;
    border-radius:5px;
    background-color:#fff0ea;

    display:flex;
    justify-content:center;
    align-items:center;
    flex-direction:column;
`
const Title = styled.p`
  color:#ff8b60;
  text-align:center;
  font-weight:bold;
  width:250px;
`
export const Card = () => {
  return (
    <Container>
        <Title>
          USE CODE: PIGI100 FOR RS.100 OFF ON YOUR 
          FIRST ORDER
        </Title>
        <Boton/>
    </Container>
  )
}
