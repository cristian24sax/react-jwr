import React from 'react'
import styled from 'styled-components'
const But = styled.button`
    text-align:center;
    background-color:#f97c4e;
    border-radius:25px;
    width:200px;
    border:inherit;
    cursor: pointer;
`
const Title = styled.p`
    color:#fff;
    font-weight:bold;
    text-transform:uppercase;
`
export const Boton = () => {
  return (
    <But>
        <Title>
        grab now
        </Title>
    </But>
  )
}
