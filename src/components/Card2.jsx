import React from 'react'
import styled from 'styled-components'
import { Footer } from '../submenu/Footer'
import { Options} from '../submenu/Options'

const Container = styled.div`
    background-color:${props=>props.color};
    height:auto;
    width:350px;
    padding: 10px;
    border-radius:25px;
    margin-top:15px;
`

export const Card2 = ({color,options,footer,onChange,id,onReset}) => {
    const handleChange=(value)=>{
        if(typeof onChange ==='function') onChange(value,id)
        // console.log(value)
    }
    const handleReset=(id)=>{
        if(typeof onReset ==='function') onReset(id)
        // console.log(value)
    }

    // console.log({handleChange})
    return (
    <Container color={color}>
        {
            options.map(opc=><Options key={opc.title} {...opc} onChange={handleChange}/>)
        }
        {/* onReset={} onAcept={} onCancel={} */}
        <Footer valor={footer} onReset={handleReset} id={id} />

    </Container>
  )
}
